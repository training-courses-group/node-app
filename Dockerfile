# syntax=docker/dockerfile:1

FROM node:21.4-slim

# Use production node environment by default.
# ENV NODE_ENV production

WORKDIR /srv/app

# Copy the rest of the source files into the image.
COPY . /srv/app

RUN yarn install

# Run the application.
CMD yarn start
